[@ticmakers-react-native/text-avatar](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [size](_styles_.md#size)
* [styles](_styles_.md#styles)

---

## Variables

<a id="size"></a>

### `<Const>` size

**● size**: *`60`* = 60

*Defined in styles.ts:3*

___
<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    alignItems: 'center',
    borderRadius: size / 2,
    height: size,
    justifyContent: 'center',
    width: size,
  },

  title: {
    fontSize: size / 2.5,
  },
})

*Defined in styles.ts:5*

#### Type declaration

 container: `object`

 alignItems: "center"

 borderRadius: `number`

 height: `number`

 justifyContent: "center"

 width: `number`

 title: `object`

 fontSize: `number`

___

