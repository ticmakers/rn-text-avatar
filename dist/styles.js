"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var size = 60;
var styles = react_native_1.StyleSheet.create({
    container: {
        alignItems: 'center',
        borderRadius: size / 2,
        height: size,
        justifyContent: 'center',
        width: size,
    },
    title: {
        fontSize: size / 2.5,
    },
});
exports.default = styles;
//# sourceMappingURL=styles.js.map