"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var tinycolor = require("tinycolor2");
var styles_1 = require("./styles");
var TextAvatar = (function (_super) {
    __extends(TextAvatar, _super);
    function TextAvatar(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    TextAvatar.prototype.render = function () {
        var _a = this._processProps(), background = _a.background, color = _a.color, size = _a.size, style = _a.style, title = _a.title, titleStyle = _a.titleStyle;
        var randColor = tinycolor.random();
        var isLight = randColor.getBrightness() > 200;
        var bIsLight = background && tinycolor(background).getBrightness() > 200 || isLight;
        var containerStyle = react_native_1.StyleSheet.flatten([
            styles_1.default.container,
            { backgroundColor: randColor.toRgbString() },
            style,
            background ? { backgroundColor: background } : {},
            size ? { width: size, height: size, borderRadius: size / 2 } : {},
        ]);
        var tStyle = react_native_1.StyleSheet.flatten([
            styles_1.default.title,
            { color: isLight ? 'black' : 'white' },
            background ? { color: bIsLight ? 'black' : 'white' } : {},
            titleStyle,
            color ? { color: color } : {},
            size ? { fontSize: size / 2.5 } : {},
        ]);
        return (React.createElement(react_native_1.View, { style: containerStyle },
            React.createElement(react_native_1.Text, { style: tStyle }, title)));
    };
    TextAvatar.prototype._processProps = function () {
        var _a = this.props, background = _a.background, color = _a.color, options = _a.options, size = _a.size, style = _a.style, title = _a.title, titleStyle = _a.titleStyle;
        var props = {
            background: (options && options.background) || (background || undefined),
            color: (options && options.color) || (color || undefined),
            size: (options && options.size) || (size || undefined),
            style: (options && options.style) || (style || undefined),
            title: (options && options.title) || (title || 'TA'),
            titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
        };
        return props;
    };
    return TextAvatar;
}(React.Component));
exports.default = TextAvatar;
//# sourceMappingURL=TextAvatar.js.map