import { StyleSheet } from 'react-native'

const size = 60

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderRadius: size / 2,
    height: size,
    justifyContent: 'center',
    width: size,
  },

  title: {
    fontSize: size / 2.5,
  },
})

export default styles
