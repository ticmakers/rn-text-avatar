import * as React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import * as tinycolor from 'tinycolor2'

import { TypeComponent } from '@ticmakers-react-native/core'
import { ITextAvatarProps, ITextAvatarState } from './../index'
import styles from './styles'

/**
 * Class to define the TextAvatar component
 * @class TextAvatar
 * @extends {React.Component<ITextAvatarProps, ITextAvatarState>}
 */
export default class TextAvatar extends React.Component<ITextAvatarProps, ITextAvatarState> {
  /**
   * Creates an instance of TextAvatar.
   * @param {ITextAvatarProps} props    An object of the TextAvatarProps
   * @memberof TextAvatar
   */
  constructor(props: ITextAvatarProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof TextAvatar
   */
  public render(): TypeComponent {
    const { background, color, size, style, title, titleStyle } = this._processProps()
    const randColor = tinycolor.random()
    const isLight = randColor.getBrightness() > 200
    const bIsLight = background && tinycolor(background).getBrightness() > 200 || isLight

    const containerStyle = StyleSheet.flatten([
      styles.container,
      { backgroundColor: randColor.toRgbString() },
      style,
      background ? { backgroundColor: background } : {},
      size ? { width: size, height: size, borderRadius: size / 2 } : {},
    ])

    const tStyle = StyleSheet.flatten([
      styles.title,
      { color: isLight ? 'black' : 'white' },
      background ? { color: bIsLight ? 'black' : 'white' } : {},
      titleStyle,
      color ? { color } : {},
      size ? { fontSize: size / 2.5 } : {},
    ])

    return (
      <View style={ containerStyle }>
        <Text style={ tStyle }>{ title }</Text>
      </View>
    )
  }

  /**
   * Method to process the props
   * @private
   * @returns {ITextAvatarState}
   * @memberof TextAvatar
   */
  private _processProps(): ITextAvatarState {
    const { background, color, options, size, style, title, titleStyle } = this.props

    const props: ITextAvatarState = {
      background: (options && options.background) || (background || undefined),
      color: (options && options.color) || (color || undefined),
      size: (options && options.size) || (size || undefined),
      style: (options && options.style) || (style || undefined),
      title: (options && options.title) || (title || 'TA'),
      titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
    }

    return props
  }
}
