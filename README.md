# TIC Makers - React Native Text Avatar
React native component for text avatar.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Text Avatar Expo's snack]()

## Install

Install `@ticmakers-react-native/text-avatar` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/text-avatar --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/text-avatar
```

## How to use?

```javascript
import React from 'react'
import TextAvatar from '@ticmakers-react-native/text-avatar'

export default class App extends React.Component {

  render() {
    return (
      <TextAvatar title="TA" />
    )
  }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| ---- | ---- | ---- | ----

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Create tests
- Add new props and components in readme
- Improve README

## Version 1.0.1 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-text-avatar/src/master/CHANGELOG.md
