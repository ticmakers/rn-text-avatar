import * as React from 'react'
import { ViewProps } from 'react-native'

import { TypeStyle, TypeComponent } from '@ticmakers-react-native/core'

/**
 * Interface to define the state of the component TextAvatar
 * @interface ITextAvatarState
 * @extends {ViewProps}
 */
export interface ITextAvatarState extends ViewProps {
  /**
   * A string color to apply in avatar (hex,rgb,rgba,etc...)
   * @type {string}
   */
  background?: string

  /**
   * A string color to apply in text (hex,rgb,rgba,etc...)
   * @type {string}
   */
  color?: string

  /**
   * A number to define the size of the avatar
   * @type {number}
   */
  size?: number

  /**
   * A string to define the text of the avatar
   * @type {string}
   */
  title: string

  /**
   * Define a custom style to the title
   * @type {TypeStyle}
   */
  titleStyle?: TypeStyle

  /**
   * Define a custom style to the component
   * @type {TypeStyle}
   */
  style?: TypeStyle
}

/**
 * Interface to define the props of the component TextAvatar
 * @interface ITextAvatarProps
 * @extends {ITextAvatarState}
 */
export interface ITextAvatarProps extends ITextAvatarState {
  /**
   * Prop for group all the props of the component
   * @type {ITextAvatarState}
   * @memberof ITextAvatarProps
   */
  options?: ITextAvatarState
}

/**
 * Class to define the TextAvatar component
 * @class TextAvatar
 * @extends {React.Component<ITextAvatarProps, ITextAvatarState>}
 */
declare class TextAvatar extends React.Component<ITextAvatarProps, ITextAvatarState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof TextAvatar
   */
  public render(): TypeComponent

  /**
   * Method to process the props
   * @private
   * @returns {ITextAvatarState}
   * @memberof TextAvatar
   */
  private _processProps(): ITextAvatarState
}

declare module '@ticmakers-react-native/text-avatar'

export default TextAvatar
